package com.company;

import java.util.concurrent.ConcurrentHashMap;
public class Main {
    static volatile ConcurrentHashMap<Integer, WeatherInfo> CHM = new ConcurrentHashMap<>();
    public static void main(String[] args) {
        String[][] words = new String[][]{{"City","temperature","pressure","humidity","rainy"},
                {"Город","температура","давление","влажность","осадки"},
                {"Stadt","Temperatur","Druck","Feuchtigkeit","regnerisch"}};
        long[] times = new long[]{1000,1500,2000};
        Thread updater = new CheckThread();
        updater.start();

        Thread[] printer = new PrintThread[3];
        for (int i = 0; i < printer.length; i++) {
            printer[i] = new PrintThread(updater, times[i],words[i]);
            printer[i].start();
        }


    }
}
