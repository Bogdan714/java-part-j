package com.company;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

public class CheckThread extends Thread {
    volatile ConcurrentHashMap<Integer, WeatherInfo> CHM;

    public CheckThread() {
        CHM = new ConcurrentHashMap<>();
        CHM.put(0, new WeatherInfo(" X",0,0,0,0));
        CHM.put(1, new WeatherInfo(" Y",0,0,0,0));
        CHM.put(2, new WeatherInfo(" Z",0,0,0,0));
    }

    public ConcurrentHashMap<Integer, WeatherInfo> getCHM() {
        return CHM;
    }

    @Override
    public void run() {
        try {
            while (isAlive()) {
                for (Iterator<WeatherInfo> iterator = CHM.values().iterator(); iterator.hasNext(); ) {
                    WeatherInfo weatherInfo = iterator.next();
                    weatherInfo.setTemperature((float)Math.random() * 75 - 25);
                    weatherInfo.setPressure((float)Math.random() * 100 + 700);
                    weatherInfo.setHumidity((float)Math.random());
                    weatherInfo.setRainy((float)Math.random());
                }
                System.out.println("\n\nUPDATED\n\n");
                Thread.sleep(3000);
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

    }
}
