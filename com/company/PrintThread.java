package com.company;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PrintThread extends Thread {
    ConcurrentHashMap<Integer, WeatherInfo> copy;
    ConcurrentHashMap<Integer, String> translate;
    Thread updater;
    long updateInterval;

    public PrintThread(Thread updater, long updateInterval, String[] forTranslation) {
        copy = new ConcurrentHashMap<>();
        this.updater = updater;
        ConcurrentHashMap<Integer, WeatherInfo> temp = ((CheckThread)updater).getCHM();
        for (Map.Entry<Integer, WeatherInfo> integerWeatherInfoEntry : temp.entrySet()) {
            copy.put(integerWeatherInfoEntry.getKey(), integerWeatherInfoEntry.getValue().getCopyData());
        }
        this.updateInterval = updateInterval;
        translate = new ConcurrentHashMap<>();
        for (int i = 0; i < forTranslation.length; i++) {
            translate.put(i, forTranslation[i]);
        }
    }

    @Override
    public void run() {
        boolean changed;
        while (isAlive()) {
            changed = false;
            ConcurrentHashMap<Integer, WeatherInfo> currentInfo = ((CheckThread)updater).getCHM();
            try {
                for (int i = 0; i < currentInfo.entrySet().size(); i++) {
                    WeatherInfo temp1 = currentInfo.get(i);
                    WeatherInfo temp2 = copy.get(i);
                    if (temp1.getRainy() != temp2.getRainy()
                            || temp1.getHumidity() != temp2.getHumidity()
                            || temp1.getPressure() != temp2.getPressure()
                            || temp1.getTemperature()!= temp2.getTemperature()) {
                        changed = true;
                        break;
                    }
                }
                if (changed) {
                    for (WeatherInfo weatherInfo : currentInfo.values()) {
                        synchronized (PrintThread.class) {
                            System.out.println(translate.get(0) + weatherInfo.getCity());
                            System.out.println(translate.get(1) + ": " + weatherInfo.getTemperature() + " C");
                            System.out.println(translate.get(2) + ": " + weatherInfo.getPressure());
                            System.out.println(translate.get(3) + ": " + (weatherInfo.getHumidity() * 100) + "%");
                            System.out.println(translate.get(4) + ": " + (weatherInfo.getRainy() * 100) + "%");
                            System.out.println("---------- ");
                        }
                    }
                    copy.clear();
                    for (Map.Entry<Integer, WeatherInfo> integerWeatherInfoEntry : currentInfo.entrySet()) {
                        copy.put(integerWeatherInfoEntry.getKey(), integerWeatherInfoEntry.getValue().getCopyData());
                    }
                }
                sleep(updateInterval);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

}
