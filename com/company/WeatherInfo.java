package com.company;

import java.util.Objects;

public class WeatherInfo {
    private String city;
    private float temperature;
    private float pressure;
    private float humidity;
    private float rainy;
    public WeatherInfo(){}
    public WeatherInfo(String city, float temperature, float pessure, float humidity, float rainy) {
        this.city = city;
        this.temperature = temperature;
        this.pressure = pessure;
        this.humidity = humidity;
        this.rainy = rainy;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public double getRainy() {
        return rainy;
    }

    public void setRainy(float rainy) {
        this.rainy = rainy;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public WeatherInfo getCopyData(){
        WeatherInfo copy = new WeatherInfo();
        copy.setCity(city);
        copy.setHumidity(humidity);
        copy.setTemperature(temperature);
        copy.setPressure(pressure);
        copy.setRainy(rainy);
        return copy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherInfo that = (WeatherInfo) o;
        return Double.compare(that.temperature, temperature) == 0 &&
                Double.compare(that.pressure, pressure) == 0 &&
                Double.compare(that.humidity, humidity) == 0 &&
                Double.compare(that.rainy, rainy) == 0 &&
                Objects.equals(city, that.city);
    }
}
